﻿using CapaAccesoDatos;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaPresentacion
{
    public partial class Administracion : Form
    {

        public Administracion()
        {
            InitializeComponent();
            
        }
        public async void poblarGridView()
        {
            HttpClient client = new HttpClient();
            string url = "http://192.168.1.163:49231/api/Usuarios";
            //string url = "http://192.168.1.87:49231/api/Usuarios";
            HttpResponseMessage res = await client.GetAsync(url);
            if (res.IsSuccessStatusCode)
            {
                var result = await res.Content.ReadAsStringAsync();
                dynamic a = JsonConvert.DeserializeObject(result);
                List<ClsUsuario> helper = new List<ClsUsuario>();

                foreach (var line in a)
                {
                    ClsUsuario usuario = new ClsUsuario();
                    usuario.Id = line.ID_USUARIO;
                    usuario.Nombre = line.NOMBRE;
                    usuario.Rut = line.RUT;

                    helper.Add(usuario);
                }
                int count = 1;
                foreach (ClsUsuario user in helper)
                {
                    int n = dataGridView1.Rows.Add();
                    dataGridView1.Rows[n].Cells[0].Value = user.Id;
                    dataGridView1.Rows[n].Cells[1].Value = user.Rut;
                    dataGridView1.Rows[n].Cells[2].Value = user.Nombre;
                    count++;
                }   

            }
            
            
        }
        private void usuariosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Administración.CrearUsuario cu = new Administración.CrearUsuario();
            cu.ShowDialog();
        }

        private async void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if(dataGridView1.Columns[e.ColumnIndex].Name == "btnVisualizar")
            {
                Administración.CrearUsuario cu = new Administración.CrearUsuario();
                cu.id = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
                cu.Show();
            }
            if (dataGridView1.Columns[e.ColumnIndex].Name == "btnEliminar")
            {
                if (MessageBox.Show("Está seguro que desea eliminar un registro?", "Eliminar un Registro", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    var id_eliminar = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
                    var url = "api/USUARIOs/" + id_eliminar;
                    HttpClient client = new HttpClient();
                    client.BaseAddress = new Uri("http://192.168.1.163:49231/");
                    //client.BaseAddress = new Uri("http://192.168.1.87:49231/");
                    HttpResponseMessage res = await client.DeleteAsync(url);
                    if (res.IsSuccessStatusCode)
                    {
                        MessageBox.Show("Se elimino el Registro de manera Exitosa!");
                    } else
                    {
                        MessageBox.Show("Ocurrio un problema al eliminar el Registro \n Contactese con el Administrador del Sistema");
                    }
                }
                
            }
            if (dataGridView1.Columns[e.ColumnIndex].Name == "btnPassword")
            {
                Administración.PopUpPassword popup = new Administración.PopUpPassword();
                popup.id = (int)dataGridView1.Rows[e.RowIndex].Cells[0].Value;
                popup.ShowDialog();
                
                ////Encripta
                //string result = string.Empty;
                //byte[] encryted = System.Text.Encoding.Unicode.GetBytes("hola");
                //result = Convert.ToBase64String(encryted);
                //Console.WriteLine(result);
                ////Desencripta
                //string desen = string.Empty;
                //byte[] decryted = Convert.FromBase64String(result);
                //desen = System.Text.Encoding.Unicode.GetString(decryted);
                //Console.WriteLine(desen);

                //MessageBox.Show(dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString());
            }

        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void Administracion_Load(object sender, EventArgs e)
        {
            poblarGridView();
        }

        private void btnActualizar_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();
            poblarGridView();
        }
    }
}
