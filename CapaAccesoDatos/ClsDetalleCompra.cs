﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaAccesoDatos
{
    public class ClsDetalleCompra
    {
        public Int32 Id { get; set; }
        public String NombreCompra { get; set; }
        public ClsCompra Compra { get; set; }
        public ClsProducto Producto { get; set; }
        public Int32 Cantidad { get; set; }
        public String UnidadMedida { get; set; }
        public Double PrecioUnitario { get; set; }
        public Double Neto { get; set; }
        public Double Iva { get; set; }

        public ClsDetalleCompra(int id, string nombreCompra, ClsCompra compra, ClsProducto producto, int cantidad, string unidadMedida, double precioUnitario, double neto, double iva)
        {
            Id = id;
            NombreCompra = nombreCompra;
            Compra = compra;
            Producto = producto;
            Cantidad = cantidad;
            UnidadMedida = unidadMedida;
            PrecioUnitario = precioUnitario;
            Neto = neto;
            Iva = iva;
        }

        public ClsDetalleCompra()
        {
            Id = 0;
            NombreCompra = null;
            Compra = null;
            Producto = null;
            Cantidad = 0;
            UnidadMedida = null;
            PrecioUnitario = 0.0;
            Neto = 0.0;
            Iva = 0.0;
        }
    }


}
