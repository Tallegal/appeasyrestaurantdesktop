﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaAccesoDatos
{
    public class ClsCompra
    {
        public Int32 Id { get; set; }
        public String NombreCompra { get; set; }
        public ClsProveedor Proveedor { get; set; }
        public CapaEntidad.ClsBodega Bodega { get; set; }
        public DateTime Fecha { get; set; }
        public Double Neto { get; set; }
        public Double Iva { get; set; }
        public Double Total { get; set; }
        public ClsDetalleCompra Detalle { get; set; }

        public ClsCompra(int id, string nombreCompra, ClsProveedor proveedor, CapaEntidad.ClsBodega bodega, DateTime fecha, double neto, double iva, double total, ClsDetalleCompra detalle)
        {
            Id = id;
            NombreCompra = nombreCompra;
            Proveedor = proveedor;
            Bodega = bodega;
            Fecha = fecha;
            Neto = neto;
            Iva = iva;
            Total = total;
            Detalle = detalle;
        }

        public ClsCompra()
        {
            Id = 0;
            NombreCompra = null;
            Proveedor = null;
            Bodega = null;
            Fecha = new DateTime();
            Neto = 0.0;
            Iva = 0.0;
            Total = 0.0;
            Detalle = null;
        }
    }
}
