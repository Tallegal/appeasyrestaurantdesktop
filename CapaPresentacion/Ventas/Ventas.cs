﻿using CapaAccesoDatos;
using CapaLogicaNegocios;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaPresentacion
{
    public partial class Ventas : Form
    {
        public Ventas()
        {
            InitializeComponent();
            ClsCliente cliente = new ClsCliente(1, "Jose", "jose@easyrestaurant.cl", "11111111-1", "54555542");
            ClsCliente cliente1 = new ClsCliente(1, "Francisco", "francisco@easyrestaurant.cl", "22222222-2", "66555542");
            ClsVenta venta = new ClsVenta(1, "PV001", cliente, 15000.0, 2850.0, 17850.0);
            ClsVenta venta1 = new ClsVenta(2, "PV002", cliente1, 15000.0, 2850.0, 17850.0);
            ClnVentas ventas = new ClnVentas();

            ventas.Agregar(venta);
            ventas.Agregar(venta1);
            var lista = ventas.Listar();
            int count = 1;
            foreach (var item in lista)
            {
                int n = dataGridView1.Rows.Add();
                dataGridView1.Rows[n].Cells[0].Value = item.NombreVenta;
                dataGridView1.Rows[n].Cells[1].Value = "PE00"+count;
                dataGridView1.Rows[n].Cells[2].Value = item.Cliente.Nombre;
                dataGridView1.Rows[n].Cells[3].Value = DateTime.Now;
                dataGridView1.Rows[n].Cells[4].Value = "Mesero 1";
                dataGridView1.Rows[n].Cells[5].Value = item.Total;
                count++;
            }
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void informesToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void crearVentaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CrearVenta cv = new CrearVenta();
            cv.ShowDialog();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void clienteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Clientes cli = new Clientes();
            cli.ShowDialog();
        }

        private void productosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Productos producto = new Productos();
            producto.ShowDialog();
        }

        private void reporteDeVentasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ReporteVentas rv = new ReporteVentas();
            rv.ShowDialog();
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void Ventas_Load(object sender, EventArgs e)
        {

        }
    }
}
