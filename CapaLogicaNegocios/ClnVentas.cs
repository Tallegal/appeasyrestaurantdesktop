﻿using CapaAccesoDatos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaLogicaNegocios
{
    public class ClnVentas : IBase<ClsVenta>
    {
        List<ClsVenta> productos = new List<ClsVenta>();

        public bool Agregar(ClsVenta objeto)
        {
            if (objeto != null)
            {
                objeto.Id = productos.Count + 1;
                productos.Add(objeto);
                return true;
            }

            return false;

        }

        public bool Eliminar(int id)
        {
            int count = 0;
            bool esta = false;
            foreach (var product in productos)
            {
                if (product.Id == id)
                {
                    esta = true;
                    break;
                }
                count++;
            }
            if (esta)
            {
                productos.RemoveAt(count);
                return true;
            }
            return false;
        }

        public List<ClsVenta> Listar()
        {
            return productos;
        }

        public bool Modificar(ClsVenta objeto)
        {
            int count = 0;
            bool esta = false;
            foreach (var product in productos)
            {
                if (product.Id == objeto.Id)
                {
                    esta = true;
                    break;
                }
                count++;
            }
            if (esta)
            {
                productos[count] = objeto;
                return true;
            }
            return false;
        }
    }
}
