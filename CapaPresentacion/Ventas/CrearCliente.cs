﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaPresentacion
{
    public partial class CrearCliente : Form
    {
        public string id;
        public HttpClient client = new HttpClient();

        public CrearCliente()
        {
            InitializeComponent();
            client.BaseAddress = new Uri("http://192.168.1.163:49231/");
            //client.BaseAddress = new Uri("http://192.168.1.87:49231/");
        }

        public static bool ValidaRut(string rut)
        {
            rut = rut.Replace(".", "").ToUpper();
            Regex expresion = new Regex("^([0-9]+-[0-9K])$");
            string dv = rut.Substring(rut.Length - 1, 1);
            if (!expresion.IsMatch(rut))
            {
                return false;
            }
            char[] charCorte = { '-' };
            string[] rutTemp = rut.Split(charCorte);
            if (dv != Digito(int.Parse(rutTemp[0])))
            {
                return false;
            }
            return true;
        }

        public static string Digito(int rut)
        {
            int suma = 0;
            int multiplicador = 1;
            while (rut != 0)
            {
                multiplicador++;
                if (multiplicador == 8)
                    multiplicador = 2;
                suma += (rut % 10) * multiplicador;
                rut = rut / 10;
            }
            suma = 11 - (suma % 11);
            if (suma == 11)
            {
                return "0";
            }
            else if (suma == 10)
            {
                return "K";
            }
            else
            {
                return suma.ToString();
            }
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private async void CrearCliente_Load(object sender, EventArgs e)
        {
            if (this.id != null)
            {
                btnEditar.Visible = true;
                btnGuardar.Visible = false;
                var url = "/api/Clientes/" + this.id;
                HttpResponseMessage message = await client.GetAsync(url);
                if (message.IsSuccessStatusCode)
                {
                    var result = await message.Content.ReadAsStringAsync();
                    Console.WriteLine(result);
                    dynamic json = JsonConvert.DeserializeObject(result);

                    lblId.Text = json.ID_CLIENTE;

                    txtNombre.Text = json.NOMBRE_CLIENTE;
                    txtApellidoP.Text = json.APELLIDOP_CLIENTE;
                    txtApellidoM.Text = json.APELLIDOM_CLIENTE;
                    txtRut.Text = json.RUT_CLIENTE;
                    txtMail.Text = json.MAIL_CLIENTE;
                    txtCelular.Text = json.TELEFONO_CLIENTE;

                    // Solo lectura
                    txtNombre.ReadOnly = true;
                    txtApellidoP.ReadOnly = true;
                    txtApellidoM.ReadOnly = true;
                    txtRut.ReadOnly = true;
                    txtMail.ReadOnly = true;
                    txtCelular.ReadOnly = true;
                }

            }
        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void tableLayoutPanel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private async void btnGuardar_Click(object sender, EventArgs e)
        {
            if (lblId.Text != "")
            {
                string nombre = txtNombre.Text;
                string apellidop = txtApellidoP.Text;
                string apellidom = txtApellidoM.Text;
                string rut = txtRut.Text;
                string mail = txtMail.Text;
                string telefono = txtCelular.Text;
                string id = lblId.Text;
                var url = "api/UpdateCliente?nombre=" + nombre + "&apellidop=" + apellidop + "&apellidom=" + apellidom + "&rut=" + rut + "&mail=" + mail + "&telefono=" + telefono + "&id=" + id;


                HttpResponseMessage res = await client.PostAsync(url, null);

                if (res.IsSuccessStatusCode)
                {
                    MessageBox.Show("El cliente se Modificó con Éxito");
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Error! Ocurrió un problema al Modificar el Registro \n Contacte con el Administrador del Sistema");
                }
            }
            else
            {
                string nombre = txtNombre.Text;
                string apellidop = txtApellidoP.Text;
                string apellidom = txtApellidoM.Text;
                string rut = txtRut.Text;
                string mail = txtMail.Text;
                string telefono = txtCelular.Text;
                string url = "api/CrearCliente?nombre=" + nombre + "&apellidop=" + apellidop + "&apellidom=" + apellidom + "&rut=" + rut + "&mail=" + mail + "&telefono=" + telefono;
                HttpResponseMessage res = await client.PostAsync(url, null);
                if (res.IsSuccessStatusCode)
                {
                    MessageBox.Show("El cliente se Registro con Exitó");
                }
                else
                {
                    MessageBox.Show("Error! Ocurrio un problema al Guardar el Registro \n Contacte el Administrador del Sistema");
                }
            }
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            btnEditar.Visible = false;
            btnGuardar.Visible = true;
            txtApellidoM.ReadOnly = false;
            txtApellidoP.ReadOnly = false;
            txtNombre.ReadOnly = false;
            txtMail.ReadOnly = false;
            txtCelular.ReadOnly = false;
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
