﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaLogicaNegocios
{
    public interface IBase<T>
    {
       
        Boolean Agregar(T objeto);
        Boolean Modificar(T objeto);
        List<T> Listar();
        Boolean Eliminar(int id);

    }
}
 