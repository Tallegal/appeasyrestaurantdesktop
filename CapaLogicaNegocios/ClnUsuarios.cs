﻿using CapaAccesoDatos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaLogicaNegocios
{
    public class ClnUsuarios : IBase<ClsUsuario>
    {
        public string login(string rut, string password)
        {
            ClsConexion conexion = new ClsConexion();
            return conexion.getLoginUser(rut, password).Result.ToString();
        }
        public bool Agregar(ClsUsuario objeto)
        {
            throw new NotImplementedException();
        }

        public bool Eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public List<ClsUsuario> Listar()
        {
            throw new NotImplementedException();
        }

        public bool Modificar(ClsUsuario objeto)
        {
            throw new NotImplementedException();
        }
    }
}
