﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaAccesoDatos
{
    public class ClsCliente
    {
        public Int32 Id { get; set; }
        public String Nombre { get; set; }
        public String Email { get; set; }
        public String Rut { get; set; }
        public String Telefono { get; set; }

        public ClsCliente(int id, string nombre, string email, string rut, string telefono)
        {
            Id = id;
            Nombre = nombre;
            Email = email;
            Rut = rut;
            Telefono = telefono;
        }

        public ClsCliente()
        {
            Id = 0;
            Nombre = null;
            Email = null;
            Rut = null;
            Telefono = null;
        }
    }
}
