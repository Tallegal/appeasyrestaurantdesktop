﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaPresentacion.Administración
{
    public partial class PopUpPassword : Form
    {
        public int id;
        public HttpClient client = new HttpClient();

        public PopUpPassword()
        {
            InitializeComponent();
            client.BaseAddress = new Uri("http://192.168.1.163:49231/");
            //client.BaseAddress = new Uri("http://192.168.1.87:49231/");
        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private async void btnConfirmar_Click(object sender, EventArgs e)
        {
            //Encripta
            string entrada = txtNuevaContra.Text;
            string result = string.Empty;
            byte[] encryted = System.Text.Encoding.Unicode.GetBytes(entrada);
            result = Convert.ToBase64String(encryted);
            
            var url = "api/UpdatePassword?id="+this.id+"&password="+result;
            HttpResponseMessage res = await client.PostAsync(url, null);

            if (res.IsSuccessStatusCode)
            {
                MessageBox.Show("La Contraseña se Modificó con éxito");
                this.Close();
            }
            else
            {
                MessageBox.Show("Error! Ocurrió un problema al Modificar la Contraseña \n Contacte con el Administrador del Sistema");
            }
        }

        private void PopUpPassword_Load(object sender, EventArgs e)
        {

        }
    }
}
