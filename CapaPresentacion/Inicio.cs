﻿using CapaAccesoDatos;
using CapaLogicaNegocios;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace CapaPresentacion
{
    public partial class Inicio : Form
    {
        public ClsUsuario usuario_logeado;

        public Inicio()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Administracion admin = new Administracion();
            admin.ShowDialog();
        }

        private void btnVentas_Click(object sender, EventArgs e)
        {
            Ventas venta = new Ventas();
            venta.ShowDialog();
        }

        private void btnCompras_Click(object sender, EventArgs e)
        {
            Compras compra = new Compras();
            compra.ShowDialog();
        }

        private void btnFinanzas_Click(object sender, EventArgs e)
        {
            Finanzas finanza = new Finanzas();
            finanza.ShowDialog();
        }

        private void btnBodega_Click(object sender, EventArgs e)
        {
            Bodega bodega = new Bodega();
            bodega.ShowDialog();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void tableLayoutPanel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnCocina_Click(object sender, EventArgs e)
        {
            Cocina.Cocina cocina = new Cocina.Cocina();
            cocina.ShowDialog();
        }

        private void Inicio_Load(object sender, EventArgs e)
        {
            Console.WriteLine(usuario_logeado.Nombre);
            if(usuario_logeado.Venta != "1")
            {
                btnVentas.Visible = false;
            }
            if (usuario_logeado.Compra != "1")
            {
                btnCompras.Visible = false;
            }
            if (usuario_logeado.Finanzas != "1")
            {
                btnFinanzas.Visible = false;
            }
            if (usuario_logeado.Bodega != "1")
            {
                btnBodega.Visible = false;
            }
            if (usuario_logeado.Cocina != "1")
            {
                btnCocina.Visible = false;
            }
            if (usuario_logeado.Administracion != "1")
            {
                btnAdministrador.Visible = false;
            }
            lblNombreUsuario.Text = usuario_logeado.Nombre;

        }
    }
}
