﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaPresentacion
{
    public partial class CrearBodega : Form
    {
        public string id;
        public HttpClient client = new HttpClient();

        public CrearBodega()
        {
            InitializeComponent();
            client.BaseAddress = new Uri("http://192.168.1.163:49231/");
            //client.BaseAddress = new Uri("http://192.168.1.87:49231/");
        }

        private async void CrearBodega_Load(object sender, EventArgs e)
        {
            if (this.id != null)
            {
                btnEditar.Visible = true;
                btnGuardar.Visible = false;
                var url = "/api/Bodegas/" + this.id;
                HttpResponseMessage message = await client.GetAsync(url);
                if (message.IsSuccessStatusCode)
                {
                    var result = await message.Content.ReadAsStringAsync();
                    Console.WriteLine(result);
                    dynamic json = JsonConvert.DeserializeObject(result);

                    lblId.Text = json.ID_BODEGA;

                    txtNombre.Text = json.NOMBRE_BODEGA;
                    txtCódigoCorto.Text = json.UBICACION_STOCK;
                    txtDireccion.Text = json.DIRECCION;
                    txtComuna.Text = json.COMUNA;

                    // Solo lectura
                    txtNombre.ReadOnly = true;
                    txtCódigoCorto.ReadOnly = true;
                    txtDireccion.ReadOnly = true;
                    txtComuna.ReadOnly = true;
                }

            }
        }

        private async void btnGuardar_Click(object sender, EventArgs e)
        {
            if (lblId.Text != "")
            {
                string nombre = txtNombre.Text;
                string codigo_corto = txtCódigoCorto.Text;
                string comuna = txtComuna.Text;
                string direccion = txtDireccion.Text;
                string id = lblId.Text;
                var url = "api/UpdateBodega?nombre=" + nombre + "&ubicacion=" + codigo_corto + "&direccion=" + direccion + "&comuna=" + comuna +"&id=" + id;


                HttpResponseMessage res = await client.PostAsync(url, null);

                if (res.IsSuccessStatusCode)
                {
                    MessageBox.Show("La bodega se Modificó con Éxito");
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Error! Ocurrió un problema al Modificar el Registro \n Contacte con el Administrador del Sistema");
                }
            }
            else
            {
                string nombre = txtNombre.Text;
                string codigo_corto = txtCódigoCorto.Text;
                string comuna = txtComuna.Text;
                string direccion = txtDireccion.Text;
                string url = "api/CrearBodega?nombre=" + nombre + "&ubicacion=" + codigo_corto + "&direccion=" + direccion + "&comuna=" + comuna;
                HttpResponseMessage res = await client.PostAsync(url, null);
                if (res.IsSuccessStatusCode)
                {
                    MessageBox.Show("La bodega se Registro con Exitó");
                }
                else
                {
                    MessageBox.Show("Error! Ocurrio un problema al Guardar el Registro \n Contacte el Administrador del Sistema");
                }
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            btnEditar.Visible = false;
            btnGuardar.Visible = true;
            txtNombre.ReadOnly = false;
            txtCódigoCorto.ReadOnly = false;
            txtDireccion.ReadOnly = false;
            txtComuna.ReadOnly = false;
        }
    }
}
