﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaAccesoDatos
{
    public class ClsVenta
    {
        public Int32 Id { get; set; }
        public String NombreVenta { get; set; }
        public ClsCliente Cliente { get; set; }
        public Double Neto { get; set; }
        public Double Iva { get; set; }
        public Double Total { get; set; }
        public DateTime Fecha { get; set; }

        public ClsVenta(int id, string nombreVenta, ClsCliente cliente, double neto, double iva, double total)
        {
            Id = id;
            NombreVenta = nombreVenta;
            Cliente = cliente;
            Neto = neto;
            Iva = iva;
            Total = total;
        }

        public ClsVenta()
        {
            Id = 0;
            NombreVenta = null;
            Cliente = null;
            Neto = 0;
            Iva = 0;
            Total = 0;
        }
    }
}
