﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace CapaAccesoDatos
{
    public class ClsUsuario
    {
        //Parametros
        public Int32 Id { get; set; }
        public String Nombre { get; set; }
        public String ApellidoP { get; set; }
        public String ApellidoM { get; set; }
        public String Email { get; set; }
        public String Rut { get; set; }
        public String Telefono { get; set; }
        public String Password { get; set; }
        public String Venta { get; set; }
        public String Compra { get; set; }
        public String Finanzas { get; set; }
        public String Bodega { get; set; }
        public String Cocina { get; set; }
        public String Administracion { get; set; }


        // Constructores
        public ClsUsuario(int id, string nombre, string apellidop, string apellidom, string email, string rut, string telefono, string password, string venta, string compra, string finanzas, string bodega, string cocina, string administracion)
        {
            Id = id;
            Nombre = nombre;
            ApellidoP = apellidop;
            ApellidoM = apellidom;
            Email = email;
            Rut = rut;
            Telefono = telefono;
            Password = password;
            Venta = venta;
            Compra = compra;
            Finanzas = finanzas;
            Bodega = bodega;
            Cocina = cocina;
            Administracion = administracion;
        }

        public ClsUsuario()
        {
            Id = 0;
            Nombre = null;
            ApellidoP = null;
            ApellidoM = null;
            Email = null;
            Rut = null;
            Telefono = null;
            Password = null;
            Venta = null;
            Compra = null;
            Finanzas = null;
            Bodega = null;
            Cocina = null;
            Administracion = null;
        }

    }
}
