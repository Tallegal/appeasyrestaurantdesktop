﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaAccesoDatos
{
    public class ClsProducto
    {
        public Int32 Id { get; set; }
        public String Nombre { get; set; }
        public String Descripcion { get; set; }
        public String CodigoInterno { get; set; }
        public Double Precio { get; set; }
        public Double Costo { get; set; }
        public String UnidadMedida { get; set; }
        public Boolean Venta { get; set; }
        public Boolean Compra { get; set; }
        public Int32 QtyMano { get; set; }
        public Int32 QtyReservada { get; set; }

        public ClsProducto(int id, string nombre, string descripcion, string codigoInterno, double precio, double costo, string unidadMedida, bool venta, bool compra, int qtyMano, int qtyReservada)
        {
            Id = id;
            Nombre = nombre;
            Descripcion = descripcion;
            CodigoInterno = codigoInterno;
            Precio = precio;
            Costo = costo;
            UnidadMedida = unidadMedida;
            Venta = venta;
            Compra = compra;
            QtyMano = qtyMano;
            QtyReservada = qtyReservada;
        }

        public ClsProducto()
        {
            Id = 0;
            Nombre = null;
            Descripcion = null;
            CodigoInterno = null;
            Precio = 0.0;
            Costo = 0.0;
            UnidadMedida = null;
            Venta = false;
            Compra = false;
            QtyMano = 0;
            QtyReservada = 0;
        }
    }
}
