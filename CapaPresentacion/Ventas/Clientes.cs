﻿using CapaAccesoDatos;
using CapaLogicaNegocios;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaPresentacion
{
    public partial class Clientes : Form
    {
        public HttpClient client = new HttpClient();

        public Clientes()
        {
            InitializeComponent();
            client.BaseAddress = new Uri("http://192.168.1.163:49231/");


            //ClsCliente cliente = new ClsCliente(1, "Jose", "jose@easyrestaurant.cl", "11111111-1", "54555542");
            //ClsCliente cliente1 = new ClsCliente(1, "Francisco", "francisco@easyrestaurant.cl", "22222222-2", "66555542");
            //ClnClientes client = new ClnClientes();
            //client.Agregar(cliente);
            //client.Agregar(cliente1);
            //var lista = client.Listar();
            //int count = 1;
            //foreach (var item in lista)
            //{
            //    int n = dataGridView1.Rows.Add();
            //    dataGridView1.Rows[n].Cells[0].Value = item.Nombre;
            //    dataGridView1.Rows[n].Cells[1].Value = item.Telefono;
            //    dataGridView1.Rows[n].Cells[2].Value = item.Email;
            //    count++;
            //}
        }

        public async void poblarGridView()
        {
            string url = "/api/clientes";
            HttpResponseMessage res = await client.GetAsync(url);
            if (res.IsSuccessStatusCode)
            {
                var result = await res.Content.ReadAsStringAsync();
                dynamic a = JsonConvert.DeserializeObject(result);
                List<ClsCliente> helper = new List<ClsCliente>();

                foreach (var line in a)
                {
                    Console.WriteLine(line);
                    ClsCliente cliente = new ClsCliente();
                    cliente.Id = line.ID_CLIENTE;
                    cliente.Nombre = line.NOMBRE_CLIENTE + " " + line.APELLIDOP_CLIENTE + " " + line.APELLIDOM_CLIENTE;
                    cliente.Email = line.MAIL_CLIENTE;
                    cliente.Telefono = line.TELEFONO_CLIENTE;
                   
                    helper.Add(cliente);
                }
                int count = 1;
                foreach (ClsCliente user in helper)
                {
                    int n = dataGridView1.Rows.Add();
                    dataGridView1.Rows[n].Cells[0].Value = user.Id;
                    dataGridView1.Rows[n].Cells[1].Value = user.Nombre;
                    dataGridView1.Rows[n].Cells[2].Value = user.Email;
                    dataGridView1.Rows[n].Cells[3].Value = user.Telefono;
                    count++;
                }

            }


        }

        private void flowLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnCrearCliente_Click(object sender, EventArgs e)
        {
            CrearCliente cliCrear = new CrearCliente();
            cliCrear.ShowDialog();
        }

        private void Clientes_Load(object sender, EventArgs e)
        {
            poblarGridView();
        }

        private void btnActualizar_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();
            poblarGridView();
        }

        private async void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView1.Columns[e.ColumnIndex].Name == "btnVisualizar")
            {
                CrearCliente cu = new CrearCliente();
                cu.id = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
                cu.Show();
            }
            if (dataGridView1.Columns[e.ColumnIndex].Name == "btnEliminar")
            {
                if (MessageBox.Show("Está seguro que desea eliminar un registro?", "Eliminar un Registro", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    var id_eliminar = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
                    var url = "api/clientes/" + id_eliminar;
                    HttpClient client = new HttpClient();
                    client.BaseAddress = new Uri("http://192.168.1.163:49231/");
                    //client.BaseAddress = new Uri("http://192.168.1.87:49231/");
                    HttpResponseMessage res = await client.DeleteAsync(url);
                    if (res.IsSuccessStatusCode)
                    {
                        MessageBox.Show("Se elimino el Registro de manera Exitosa!");
                    }
                    else
                    {
                        MessageBox.Show("Ocurrio un problema al eliminar el Registro \n Contactese con el Administrador del Sistema");
                    }
                }

            }
        }
    }
}
