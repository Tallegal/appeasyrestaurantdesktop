﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaAccesoDatos;
using CapaLogicaNegocios;
using Newtonsoft.Json;

namespace CapaPresentacion
{
    public partial class CrearProducto : Form
    {
        public string id;
        public HttpClient client = new HttpClient();

        public CrearProducto()
        {
            InitializeComponent();
            client.BaseAddress = new Uri("http://192.168.1.163:49231/");
            //client.BaseAddress = new Uri("http://192.168.1.87:49231/");
        }

        private async void btnGuardar_Click(object sender, EventArgs e)
        {
        
            if (lblId.Text != "")
            {
                string nombre = txtNombre.Text;
                double precio = Double.Parse(txtPrecioUnitario.Text);
                string unidadMedida = cmbUnidadMedida.Text;
                string venta;
                string compra;

                if(chkVenta.Checked)
                {
                    venta = "1";
                }
                else
                {
                    venta = "0";
                }
                if (chkCompra.Checked)
                {
                    compra = "1";
                }
                else
                {
                    compra = "0";
                }

                string codigoInterno = txtCodigo.Text;
                double costo = Double.Parse(txtCosto.Text);
                string descripcion = txtDescripcion.Text;
                string stock = "0";
                string tipoProductoBodega = "";
                string id = lblId.Text;
                var url = "api/UpdateProductoBodega?id="+id+"&nombre="+nombre+"&descripcion="+descripcion+"&codigo_interno="+codigoInterno+"&precio="+precio+"&costo="+costo+"&unidad_medida="+unidadMedida+"&stock="+stock+"&venta="+venta+"&compra="+compra+"&tipo_producto_bodega="+ tipoProductoBodega;


                HttpResponseMessage res = await client.PostAsync(url, null);

                if (res.IsSuccessStatusCode)
                {
                    MessageBox.Show("El Producto se Modificó con Éxito");
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Error! Ocurrió un problema al Modificar el Registro \n Contacte con el Administrador del Sistema");
                }
            }
            else
            {
    
                string nombre = txtNombre.Text;
                double precio = Double.Parse(txtPrecioUnitario.Text);
                string unidadMedida = cmbUnidadMedida.Text;
                string venta;
                string compra;
                if (chkVenta.Checked)
                {
                    venta = "1";
                }
                else
                {
                    venta = "0";
                }

                if (chkCompra.Checked)
                {
                    compra = "1";
                }
                else
                {
                    compra = "0";
                }
                string codigoInterno = txtCodigo.Text;
                string result_dos = null;
                string url_existe = "api/ExisteCodigo?codigo_interno=" + codigoInterno;
                HttpResponseMessage res = await client.GetAsync(url_existe);
                if (res.IsSuccessStatusCode)
                {
                    result_dos = await res.Content.ReadAsStringAsync();
                }
                if (int.Parse(result_dos) == 0)
                {
                    double costo = Double.Parse(txtCosto.Text);
                    string descripcion = txtDescripcion.Text;
                    string stock = "0";
                    string tipoProductoBodega = "";
                    var url = "api/CrearProductoBodega?nombre=" + nombre + "&descripcion=" + descripcion + "&codigo_interno=" + codigoInterno + "&precio=" + precio + "&costo=" + costo + "&unidad_medida=" + unidadMedida + "&stock=" + stock + "&venta=" + venta + "&compra=" + compra + "&tipo_producto_bodega=" + tipoProductoBodega;

                    HttpResponseMessage res_dos = await client.PostAsync(url, null);
                    if (res_dos.IsSuccessStatusCode)
                    {
                        MessageBox.Show("El Producto se Registro con Exitó");
                    }
                    else
                    {
                        MessageBox.Show("Error! Ocurrio un problema al Guardar el Registro \n Contacte el Administrador del Sistema");
                    }
                }
                else
                {
                    MessageBox.Show("Error! Código interno ya esta registrado en el sistema.");
                }

                
            }

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private async void CrearProducto_Load(object sender, EventArgs e)
        {
            if (this.id != null)
            {
                btnEditar.Visible = true;
                btnGuardar.Visible = false;
                var url = "/api/PRODUCTOS_BODEGA/" + this.id;
                HttpResponseMessage message = await client.GetAsync(url);
                if (message.IsSuccessStatusCode)
                {
                    var result = await message.Content.ReadAsStringAsync();
                    Console.WriteLine(result);
                    dynamic json = JsonConvert.DeserializeObject(result);

                    lblId.Text = json.ID_PRODUCTOS_BODEGA;
                    txtNombre.Text = json.NOMBRE_PRODUCTO_BODEGA;
                    txtDescripcion.Text = json.DESCRIPCION;
                    txtPrecioUnitario.Text = json.PRECIO;
                    cmbUnidadMedida.Text = json.UNIDAD_MEDIDA;
                    if (json.VENTA == "1")
                    {
                        chkVenta.Checked = true;
                    }
                    else
                    {
                        chkVenta.Checked = false;
                    }
                    if(json.COMPRA == "1")
                    {
                        chkCompra.Checked = true;
                    }
                    else
                    {
                        chkCompra.Checked = false;
                    }
                    
                    txtCodigo.Text = json.CODIGO_INTERNO;
                    txtCosto.Text = json.COSTO;
              

                    // Solo lectura                 
                    txtNombre.ReadOnly = true;
                    txtDescripcion.ReadOnly = true;
                    txtPrecioUnitario.ReadOnly = true;
                    cmbUnidadMedida.Enabled = true;
                    chkVenta.Enabled = true;
                    chkCompra.Enabled = true;
                    txtCodigo.ReadOnly = true;
                    txtCosto.ReadOnly = true;
                }

            }
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            btnEditar.Visible = false;
            btnGuardar.Visible = true;
            txtNombre.ReadOnly = false;
            txtDescripcion.ReadOnly = false;
            txtPrecioUnitario.ReadOnly = false;
            cmbUnidadMedida.Enabled = false;
            chkVenta.Enabled = false;
            chkCompra.Enabled = false;
            txtCodigo.ReadOnly = true;
            txtCosto.ReadOnly = false;
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
