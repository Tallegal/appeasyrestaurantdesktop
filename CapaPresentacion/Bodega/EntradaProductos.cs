﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaPresentacion
{
    public partial class EntradaProductos : Form
    {
        public EntradaProductos()
        {
            InitializeComponent();
        }

        private void btnCrearEntrada_Click(object sender, EventArgs e)
        {
            MovimientoBodega mb = new MovimientoBodega();
            mb.ShowDialog();
        }
    }
}
