﻿using CapaAccesoDatos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaLogicaNegocios
{
    class ClnCompras : IBase<ClsCompra>
    {
        List<ClsCompra> productos = new List<ClsCompra>();

        public bool Agregar(ClsCompra objeto)
        {
            if (objeto != null)
            {
                objeto.Id = productos.Count + 1;
                productos.Add(objeto);
                return true;
            }

            return false;

        }

        public bool Eliminar(int id)
        {
            int count = 0;
            bool esta = false;
            foreach (var product in productos)
            {
                if (product.Id == id)
                {
                    esta = true;
                    break;
                }
                count++;
            }
            if (esta)
            {
                productos.RemoveAt(count);
                return true;
            }
            return false;
        }

        public List<ClsCompra> Listar()
        {
            return productos;
        }

        public bool Modificar(ClsCompra objeto)
        {
            int count = 0;
            bool esta = false;
            foreach (var product in productos)
            {
                if (product.Id == objeto.Id)
                {
                    esta = true;
                    break;
                }
                count++;
            }
            if (esta)
            {
                productos[count] = objeto;
                return true;
            }
            return false;
        }
    }
}
