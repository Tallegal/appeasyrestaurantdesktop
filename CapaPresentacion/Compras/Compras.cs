﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaEntidad;
using CapaAccesoDatos;
using Newtonsoft.Json;
using System.Net.Http;

namespace CapaPresentacion
{
    public partial class Compras : Form
    {
        public HttpClient client = new HttpClient();

        public Compras()
        {
            InitializeComponent();
            client.BaseAddress = new Uri("http://192.168.1.163:49231/");
        }

        public async void poblarGridView()
        {
            string url = "/api/compras";
            HttpResponseMessage res = await client.GetAsync(url);
            if (res.IsSuccessStatusCode)
            {
                var result = await res.Content.ReadAsStringAsync();
                dynamic a = JsonConvert.DeserializeObject(result);
                List<ClsCompra> helper = new List<ClsCompra>();

                foreach (var line in a)
                {
                    ClsCompra compra = new ClsCompra();
                    compra.Id = line.ID_BODEGA;
                    compra.NombreCompra = line.NOMBRE;
                    compra.Proveedor = line.PROVEEDOR;
                    compra.Bodega = line.BODEGA;
                    compra.Fecha = line.FECHA;
                    compra.Neto = line.IMPONIBLE;
                    compra.Iva = line.IMPUESTO;
                    compra.Total = line.TOTAL;
                    helper.Add(compra);
                }
                int count = 1;
                foreach (ClsCompra user in helper)
                {
                    int n = dataGridView1.Rows.Add();
                    dataGridView1.Rows[n].Cells[0].Value = user.Id;
                    dataGridView1.Rows[n].Cells[1].Value = user.NombreCompra;
                    dataGridView1.Rows[n].Cells[2].Value = user.Proveedor;
                    dataGridView1.Rows[n].Cells[3].Value = user.Fecha;
                    dataGridView1.Rows[n].Cells[4].Value = user.Total;
                    count++;
                }

            }


        }

        private void reporteDeVentasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ReporteCompras rc = new ReporteCompras();
            rc.ShowDialog();
        }

        private void crearVentaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CrearCompra crearCompra = new CrearCompra();
            crearCompra.ShowDialog();
        }

        private void clienteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Proveedores prov = new Proveedores();
            prov.ShowDialog();
        }

        private void productosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Productos prod = new Productos();
            prod.ShowDialog();
        }

        private void Compras_Load(object sender, EventArgs e)
        {

        }

        private async void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView1.Columns[e.ColumnIndex].Name == "btnVisualizar")
            {
                CrearCompra cu = new CrearCompra();
                cu.id = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
                cu.Show();
            }
            if (dataGridView1.Columns[e.ColumnIndex].Name == "btnEliminar")
            {
                if (MessageBox.Show("Está seguro que desea eliminar un registro?", "Eliminar un Registro", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    var id_eliminar = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
                    string url = "api/compras/" + id_eliminar;
                    HttpResponseMessage res = await client.DeleteAsync(url);
                    if (res.IsSuccessStatusCode)
                    {
                        MessageBox.Show("Se elimino el Registro de manera Exitosa!");
                    }
                    else
                    {
                        MessageBox.Show("Ocurrio un problema al eliminar el Registro \n Contactese con el Administrador del Sistema");
                    }
                }

            }
        }
    }
}
