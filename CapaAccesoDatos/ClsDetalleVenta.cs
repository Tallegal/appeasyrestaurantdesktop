﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaAccesoDatos
{
    public class ClsDetalleVenta
    {
        public Int32 Id { get; set; }
        public String NombreVenta { get; set; }
        public ClsVenta Venta { get; set; }
        public ClsProducto Producto { get; set; }
        public Int32 Cantidad { get; set; }
        public String UnidadMedida { get; set; }
        public Double PrecioUnitario { get; set; }
        public Double Neto { get; set; }
        public Double Iva { get; set; }
        public Double Total { get; set; }

        public ClsDetalleVenta(int id, string nombreVenta, ClsVenta venta, ClsProducto producto, int cantidad, string unidadMedida, double precioUnitario, double neto, double iva, double total)
        {
            Id = id;
            NombreVenta = nombreVenta;
            Venta = venta;
            Producto = producto;
            Cantidad = cantidad;
            UnidadMedida = unidadMedida;
            PrecioUnitario = precioUnitario;
            Neto = neto;
            Iva = iva;
            Total = total;
        }
        
        public ClsDetalleVenta()
        {
            Id = 0;
            NombreVenta = null;
            Venta = null;
            Producto = null;
            Cantidad = 0;
            UnidadMedida = null;
            PrecioUnitario = 0.0;
            Neto = 0.0;
            Iva = 0.0;
            Total = 0.0;
        }
    }
}
