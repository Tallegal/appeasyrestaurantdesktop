﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaPresentacion
{
    public partial class Finanzas : Form
    {
        public Finanzas()
        {
            InitializeComponent();
        }

        private void sesionesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CrearCaja caja = new CrearCaja();
            caja.ShowDialog();
        }

        private void sesionesToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Sesiones sesion = new Sesiones();
            sesion.ShowDialog();
        }

        private void bancosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CrearBanco crea = new CrearBanco();
            crea.ShowDialog();
        }

        private void bancosToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Bancos banco = new Bancos();
            banco.ShowDialog();
        }

        private void reporteDeBancoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ReporteBancos rb = new ReporteBancos();
            rb.ShowDialog();
        }

        private void reporteDeSesionesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ReporteSesiones rs = new ReporteSesiones();
            rs.ShowDialog();
        }
    }
}
