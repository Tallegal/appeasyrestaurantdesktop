﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaPresentacion
{
    public partial class Bodega : Form
    {
        public Bodega()
        {
            InitializeComponent();
        }

        private void crearProductoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Productos prod = new Productos();
            prod.ShowDialog();
        }

        private void entradaDeProductosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Bodegas bodegas = new Bodegas();
            bodegas.ShowDialog();
        }

        private void salidaDeProductosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EntradaProductos ep = new EntradaProductos();
            ep.ShowDialog();
        }

        private void productosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SalidaProductos sp = new SalidaProductos();
            sp.ShowDialog();
        }

        private void reporteProductosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ReporteProductos rp = new ReporteProductos();
            rp.ShowDialog();
        }

        private void reporteEntradasDeProductosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ReporteEntradaProductos rep = new ReporteEntradaProductos();
            rep.ShowDialog();
        }

        private void reporteDeSalidasDeProductosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ReporteSalidaProductos rsp = new ReporteSalidaProductos();
            rsp.ShowDialog();
        }

        private void Bodega_Load(object sender, EventArgs e)
        {

        }
    }
}
