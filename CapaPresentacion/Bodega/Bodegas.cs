﻿using CapaEntidad;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaPresentacion
{
    public partial class Bodegas : Form
    {
        public HttpClient client = new HttpClient();

        public Bodegas()
        {
            InitializeComponent();
            client.BaseAddress = new Uri("http://192.168.1.163:49231/");
            //client.BaseAddress = new Uri("http://192.168.1.87:49231/");
        }

        public async void poblarGridView()
        {
            string url = "/api/bodegas";
            HttpResponseMessage res = await client.GetAsync(url);
            if (res.IsSuccessStatusCode)
            {
                var result = await res.Content.ReadAsStringAsync();
                dynamic a = JsonConvert.DeserializeObject(result);
                List<ClsBodega> helper = new List<ClsBodega>();

                foreach (var line in a)
                {
                    ClsBodega bodega = new ClsBodega();
                    bodega.Id = line.ID_BODEGA;
                    bodega.Nombre = line.NOMBRE_BODEGA;
                    bodega.CodigoCorto = line.UBICACION_STOCK;
                    bodega.Direccion = line.DIRECCION;
                    bodega.Comuna = line.COMUNA;
                    helper.Add(bodega);
                }
                int count = 1;
                foreach (ClsBodega user in helper)
                {
                    int n = dataGridView1.Rows.Add();
                    dataGridView1.Rows[n].Cells[0].Value = user.Id;
                    dataGridView1.Rows[n].Cells[1].Value = user.Nombre;
                    dataGridView1.Rows[n].Cells[2].Value = user.CodigoCorto;
                    dataGridView1.Rows[n].Cells[3].Value = user.Direccion;
                    dataGridView1.Rows[n].Cells[4].Value = user.Comuna;
                    count++;
                }

            }


        }

        private void btnCrearBodega_Click(object sender, EventArgs e)
        {
            CrearBodega cb = new CrearBodega();
            cb.ShowDialog();
        }

        private void Bodegas_Load(object sender, EventArgs e)
        {
            poblarGridView();
        }

        private async void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView1.Columns[e.ColumnIndex].Name == "btnVisualizar")
            {
                CrearBodega cu = new CrearBodega();
                cu.id = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
                cu.Show();
            }
            if (dataGridView1.Columns[e.ColumnIndex].Name == "btnEliminar")
            {
                if (MessageBox.Show("Está seguro que desea eliminar un registro?", "Eliminar un Registro", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    var id_eliminar = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
                    string url = "api/bodegas/" + id_eliminar;
                    HttpResponseMessage res = await client.DeleteAsync(url);
                    if (res.IsSuccessStatusCode)
                    {
                        MessageBox.Show("Se elimino el Registro de manera Exitosa!");
                    }
                    else
                    {
                        MessageBox.Show("Ocurrio un problema al eliminar el Registro \n Contactese con el Administrador del Sistema");
                    }
                }

            }
        }

        private void btnActualizar_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();
            poblarGridView();
        }
    }
}
