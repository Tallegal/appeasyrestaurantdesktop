﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidad
{
    public class ClsBodega
    {
        public Int32 Id { get; set; }
        public String Nombre { get; set; }
        public String CodigoCorto { get; set; }
        public String Direccion { get; set; }
        public String Comuna { get; set; }

        public ClsBodega(int id, string nombre, string codigoCorto, string direccion, string comuna)
        {
            Id = id;
            Nombre = nombre;
            CodigoCorto = codigoCorto;
            Direccion = direccion;
            Comuna = comuna;
        }

        public ClsBodega()
        {
            Id = 0;
            Nombre = null;
            CodigoCorto = null;
            Direccion = null;
            Comuna = null;
        }
    }
}
