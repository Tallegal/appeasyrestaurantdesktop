﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaAccesoDatos
{
    public class ClsProveedor
    {
        public Int32 Id { get; set; }
        public String Nombre { get; set; }
        public String RazonSocial { get; set; }
        public String Giro { get; set; }
        public String Email { get; set; }
        public String Rut { get; set; }
        public String Telefono { get; set; }
        public String Direccion { get; set; }

        public ClsProveedor(int id, string nombre, string razonSocial, string giro, string email, string rut, string telefono, string direccion)
        {
            Id = id;
            Nombre = nombre;
            RazonSocial = razonSocial;
            Giro = giro;
            Email = email;
            Rut = rut;
            Telefono = telefono;
            Direccion = direccion;
        }

        public ClsProveedor()
        {
            Id = 0;
            Nombre = null;
            RazonSocial = null;
            Giro = null;
            Email = null;
            Rut = null;
            Telefono = null;
            Direccion = null;
        }
    }
}
