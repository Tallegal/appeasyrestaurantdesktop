﻿using CapaAccesoDatos;
using CapaLogicaNegocios;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaPresentacion
{
    public partial class Productos : Form
    {
        public HttpClient client = new HttpClient();

        public Productos()
        {
            InitializeComponent();
            client.BaseAddress = new Uri("http://192.168.1.163:49231/");
            //client.BaseAddress = new Uri("http://192.168.1.87:49231/");
        }

        public async void poblarGridView()
        {
            string url = "/api/productos_bodega";
            HttpResponseMessage res = await client.GetAsync(url);
            if (res.IsSuccessStatusCode)
            {
                var result = await res.Content.ReadAsStringAsync();
                dynamic a = JsonConvert.DeserializeObject(result);
                List<ClsProducto> helper = new List<ClsProducto>();
               
                foreach (var line in a)
                {
                    Console.WriteLine(line);
                    ClsProducto productoBodega = new ClsProducto();
                    productoBodega.Id = line.ID_PRODUCTOS_BODEGA;
                    productoBodega.Nombre = line.NOMBRE_PRODUCTO_BODEGA;
                    productoBodega.Precio = line.PRECIO;
                    productoBodega.QtyMano = line.STOCK_PRODUCTO_BODEGA;
                    productoBodega.UnidadMedida = line.UNIDAD_MEDIDA;
                    if (line.venta == "0")
                    {
                        productoBodega.Venta = false;
                    }
                    if (line.VENTA == "1")
                    {
                        productoBodega.Venta = true;
                    }

                    if (line.COMPRA == "0")
                    {
                        productoBodega.Compra = false;
                    }
                    if (line.compra == "1")
                    {
                        productoBodega.Compra = true;
                    }
                    productoBodega.Costo = line.COSTO;
                    productoBodega.CodigoInterno = line.CODIGO_INTERNO;
                    productoBodega.Descripcion = line.DESCRIPCION;
                    helper.Add(productoBodega);
                }
                int count = 1;
                foreach (ClsProducto user in helper)
                {
                    int n = dataGridView1.Rows.Add();
                    dataGridView1.Rows[n].Cells[0].Value = user.Id;
                    dataGridView1.Rows[n].Cells[1].Value = user.CodigoInterno;
                    dataGridView1.Rows[n].Cells[2].Value = user.Nombre;
                    dataGridView1.Rows[n].Cells[3].Value = user.Precio;
                    dataGridView1.Rows[n].Cells[4].Value = user.Costo;
                    dataGridView1.Rows[n].Cells[5].Value = user.QtyMano;
                    dataGridView1.Rows[n].Cells[6].Value = "0";
                    count++;
                }

            }


        }

        private void tableLayoutPanel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnCrearCliente_Click(object sender, EventArgs e)
        {
            CrearProducto cp = new CrearProducto();
            cp.ShowDialog();
        }

        private async void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView1.Columns[e.ColumnIndex].Name == "btnVisualizar")
            {
                CrearProducto cu = new CrearProducto();
                cu.id = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
                cu.Show();
            }
            if (dataGridView1.Columns[e.ColumnIndex].Name == "btnEliminar")
            {
                if (MessageBox.Show("Está seguro que desea eliminar un registro?", "Eliminar un Registro", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    var id_eliminar = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
                    string url = "api/productos_bodega/" + id_eliminar;
                    HttpResponseMessage res = await client.DeleteAsync(url);
                    if (res.IsSuccessStatusCode)
                    {
                        MessageBox.Show("Se elimino el Registro de manera Exitosa!");
                    }
                    else
                    {
                        MessageBox.Show("Ocurrio un problema al eliminar el Registro \n Contactese con el Administrador del Sistema");
                    }
                }

            }
            
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void btnActualizar_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();
            poblarGridView();
        }

        private void Productos_Load(object sender, EventArgs e)
        {
            poblarGridView();
        }
    }
}
