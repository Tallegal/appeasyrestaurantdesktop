﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaPresentacion
{
    public partial class CrearProveedor : Form
    {
        public string id;
        public HttpClient client = new HttpClient();

        public CrearProveedor()
        {
            InitializeComponent();
            client.BaseAddress = new Uri("http://192.168.1.163:49231/");
        }

        private async void CrearProveedor_Load(object sender, EventArgs e)
        {
            if (this.id != null)
            {
                btnEditar.Visible = true;
                btnGuardar.Visible = false;
                var url = "/api/Proveedores/" + this.id;
                HttpResponseMessage message = await client.GetAsync(url);
                if (message.IsSuccessStatusCode)
                {
                    var result = await message.Content.ReadAsStringAsync();
                    Console.WriteLine(result);
                    dynamic json = JsonConvert.DeserializeObject(result);

                    lblId.Text = json.ID_PROVEEDOR;

                    string[] nombre_split = json.NOMBRE_PROVEEDOR.ToString().Split(' ');
                    txtNombre.Text = nombre_split[0];
                    if (nombre_split.Length > 1)
                    {
                        txtApellidoM.Text = nombre_split[1];
                    }
                    if (nombre_split.Length > 2)
                    {
                        txtApellidoP.Text = nombre_split[2];
                    }
                    txtRut.Text = json.RUT_PROVEEDOR;
                    txtMail.Text = json.MAIL_PROVEEDOR;
                    txtCelular.Text = json.TELEFONO_PROVEEDOR;
                    txtDireccion.Text = json.DIRECCION_PROVEEDOR;

                    // Solo lectura
                    txtNombre.ReadOnly = true;
                    txtApellidoP.ReadOnly = true;
                    txtApellidoM.ReadOnly = true;
                    txtRut.ReadOnly = true;
                    txtMail.ReadOnly = true;
                    txtCelular.ReadOnly = true;
                    txtDireccion.ReadOnly = true;
                }

            }
        }

        private async void btnGuardar_Click(object sender, EventArgs e)
        {
            if (lblId.Text != "")
            {
                string nombre = txtNombre.Text + " " + txtApellidoP.Text + " " + txtApellidoM.Text;
                string rut = txtRut.Text;
                string mail = txtMail.Text;
                string telefono = txtCelular.Text;
                string direccion = txtDireccion.Text;
                string id = lblId.Text;
                var url = "api/UpdateProveedor?nombre=" + nombre +  "&rut=" + rut + "&mail=" + mail + "&telefono=" + telefono + "&direccion="+ direccion + "&id=" + id;


                HttpResponseMessage res = await client.PostAsync(url, null);

                if (res.IsSuccessStatusCode)
                {
                    MessageBox.Show("El Proveedor se Modificó con Éxito");
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Error! Ocurrió un problema al Modificar el Registro \n Contacte con el Administrador del Sistema");
                }
            }
            else
            {
                string nombre = txtNombre.Text + " " +txtApellidoP.Text + " " +txtApellidoM.Text;
                string rut = txtRut.Text;
                string mail = txtMail.Text;
                string telefono = txtCelular.Text;
                string direccion = txtDireccion.Text;
                string url = "api/CrearProveedor?nombre=" + nombre + "&rut=" + rut + "&mail=" + mail + "&telefono=" + telefono + "&direccion=" + direccion;
                HttpResponseMessage res = await client.PostAsync(url, null);
                if (res.IsSuccessStatusCode)
                {
                    MessageBox.Show("El proveedor se Registro con Exitó");
                }
                else
                {
                    MessageBox.Show("Error! Ocurrio un problema al Guardar el Registro \n Contacte el Administrador del Sistema");
                }
            }
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
