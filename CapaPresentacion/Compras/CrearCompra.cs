﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaAccesoDatos;
using CapaEntidad;

namespace CapaPresentacion
{
    public partial class CrearCompra : Form
    {
        public string id;
        public HttpClient client = new HttpClient();

        public CrearCompra()
        {
            InitializeComponent();
            client.BaseAddress = new Uri("http://192.168.1.163:49231/");
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private async void CrearCompra_Load(object sender, EventArgs e)
        {
            if (this.id != null)
            {
                btnEditar.Visible = true;
                btnAgregar.Visible = false;
                var url = "/api/compras/" + this.id;
                HttpResponseMessage message = await client.GetAsync(url);
                if (message.IsSuccessStatusCode)
                {
                    var result = await message.Content.ReadAsStringAsync();
                    Console.WriteLine(result);
                    dynamic json = JsonConvert.DeserializeObject(result);

                    lblId.Text = json.ID;

                    var url_prov = "/api/proveedores/" + json.PROVEEDOR;
                    HttpResponseMessage new_mess = await client.GetAsync(url_prov);
                    if (new_mess.IsSuccessStatusCode)
                    {
                        var result_prov = await new_mess.Content.ReadAsStringAsync();
                        Console.WriteLine(result_prov);
                        dynamic json_prov = JsonConvert.DeserializeObject(result_prov);
                        ClsProveedor proveedor = new ClsProveedor();
                        proveedor.Id = json_prov.ID_PROVEEDOR;
                        proveedor.Nombre = json_prov.NOMBRE_PROVEEDOR;
                        proveedor.Rut = json_prov.RUT_PROVEEDOR;
                        proveedor.Email = json_prov.MAIL_PROVEEODR;
                        proveedor.Telefono = json_prov.TELEFONO_PROVEEDOR;

                        cmbProveedor.Items.Add(proveedor.Nombre);

                    }
                    else
                    {
                        MessageBox.Show("El Proveedor no existe");
                        this.Close();
                    }
                    var url_bod = "/api/bodegas/" + json.BODEGA;
                    HttpResponseMessage new_mess_bod = await client.GetAsync(url_bod);
                    if (new_mess_bod.IsSuccessStatusCode)
                    {
                        var result_bod = await message.Content.ReadAsStringAsync();
                        Console.WriteLine(result_bod);
                        dynamic json_bod = JsonConvert.DeserializeObject(result_bod);
                        ClsBodega bode = new ClsBodega();
                        bode.Id = json_bod.ID_BODEGA;
                        bode.Nombre = json_bod.NOMBRE_BODEGA;
                        bode.Direccion = json_bod.DIRECCION;
                        bode.Comuna = json_bod.COMUNA;

                        cmbBodega.Items.Add(bode.Nombre);
                    }
                    else
                    {
                        MessageBox.Show("La bodega no existe");
                        this.Close();
                    }
                    dtFecha.Value = json.FECHA;

                    // Solo lectura
                    cmbBodega.Enabled = false;
                    cmbProveedor.Enabled = false;
                    dtFecha.Enabled = false;
                    dataGridView1.Enabled = false;
                }

            }
            else
            {
                var url_prov = "/api/proveedores";
                HttpResponseMessage new_mess = await client.GetAsync(url_prov);
                if (new_mess.IsSuccessStatusCode)
                {
                    var result = await new_mess.Content.ReadAsStringAsync();
                    dynamic a = JsonConvert.DeserializeObject(result);
                    List<ClsProveedor> helper = new List<ClsProveedor>();

                    foreach (var line in a)
                    {
                        Console.WriteLine(line);
                        ClsProveedor proveedor = new ClsProveedor();
                        proveedor.Id = line.ID_PROVEEDOR;
                        proveedor.Nombre = line.NOMBRE_PROVEEDOR;
                        proveedor.Rut = line.RUT_PROVEEDOR;
                        proveedor.Email = line.MAIL_PROVEEODR;
                        proveedor.Telefono = line.TELEFONO_PROVEEDOR;

                        cmbProveedor.Items.Add(proveedor.Nombre);
                    }
                    
                    
                }

                var url_bode = "/api/bodegas";
                HttpResponseMessage mess = await client.GetAsync(url_bode);
                if (mess.IsSuccessStatusCode)
                {
                    var result = await mess.Content.ReadAsStringAsync();
                    dynamic a = JsonConvert.DeserializeObject(result);
                    List<ClsBodega> helper = new List<ClsBodega>();

                    foreach (var line in a)
                    {
                        ClsBodega bode = new ClsBodega();
                        bode.Id = line.ID_BODEGA;
                        bode.Nombre = line.NOMBRE_BODEGA;
                        bode.Direccion = line.DIRECCION;
                        bode.Comuna = line.COMUNA;

                        cmbBodega.Items.Add(bode.Nombre);
                    }


                }

                var url_bod = "/api/productos_bodega";
                HttpResponseMessage new_bod = await client.GetAsync(url_bod);
                if (new_mess.IsSuccessStatusCode)
                {
                    var result = await new_bod.Content.ReadAsStringAsync();
                    dynamic a = JsonConvert.DeserializeObject(result);

                    List<ClsProducto> helper = new List<ClsProducto>();

                    foreach (var line in a)
                    {
                        ClsProducto productoBodega = new ClsProducto();
                        productoBodega.Id = line.ID_PRODUCTOS_BODEGA;
                        productoBodega.Nombre = line.NOMBRE_PRODUCTO_BODEGA;
                        productoBodega.Precio = line.PRECIO;
                        productoBodega.QtyMano = line.STOCK_PRODUCTO_BODEGA;
                        productoBodega.UnidadMedida = line.UNIDAD_MEDIDA;
                        if (line.venta == "0")
                        {
                            productoBodega.Venta = false;
                        }
                        if (line.VENTA == "1")
                        {
                            productoBodega.Venta = true;
                        }

                        if (line.COMPRA == "0")
                        {
                            productoBodega.Compra = false;
                        }
                        if (line.compra == "1")
                        {
                            productoBodega.Compra = true;
                        }
                        productoBodega.Costo = line.COSTO;
                        productoBodega.CodigoInterno = line.CODIGO_INTERNO;
                        productoBodega.Descripcion = line.DESCRIPCION;
                        helper.Add(productoBodega);
                    }

                    ComboBox CB = new ComboBox();

                    foreach (ClsProducto prod in helper)
                    {
                        CB.Items.Add(prod.Nombre);
                    }
                    
                    ((DataGridViewComboBoxColumn)dataGridView1.Columns["cmbProducto"]).DataSource = CB.Items;

                    //foreach (var line in a)
                    //{

                    //}


                }
            }
        }

        private async void btnAgregar_Click(object sender, EventArgs e)
        {
            if (lblId.Text != "")
            {
                
                string id = lblId.Text;
                //var url = "api/UpdateProveedor?nombre=" + nombre + "&rut=" + rut + "&mail=" + mail + "&telefono=" + telefono + "&direccion=" + direccion + "&id=" + id;


                //HttpResponseMessage res = await client.PostAsync(url, null);

                //if (res.IsSuccessStatusCode)
                //{
                //    MessageBox.Show("El Proveedor se Modificó con Éxito");
                //    this.Close();
                //}
                //else
                //{
                //    MessageBox.Show("Error! Ocurrió un problema al Modificar el Registro \n Contacte con el Administrador del Sistema");
                //}
            }
            else
            {
                string proveedor = cmbProveedor.SelectedItem.ToString();
                string bodega = cmbBodega.SelectedItem.ToString();
                string fecha = dtFecha.Text;

                var url_prov = "/api/proveedores";
                HttpResponseMessage new_mess = await client.GetAsync(url_prov);
                if (new_mess.IsSuccessStatusCode)
                {
                    var result = await new_mess.Content.ReadAsStringAsync();
                    dynamic a = JsonConvert.DeserializeObject(result);
                    List<ClsProveedor> helper = new List<ClsProveedor>();

                    foreach (var line in a)
                    {
                        Console.WriteLine(line);
                        ClsProveedor prov = new ClsProveedor();
                        prov.Id = line.ID_PROVEEDOR;
                        prov.Nombre = line.NOMBRE_PROVEEDOR;
                        prov.Rut = line.RUT_PROVEEDOR;
                        prov.Email = line.MAIL_PROVEEODR;
                        prov.Telefono = line.TELEFONO_PROVEEDOR;
                        helper.Add(prov);
                    }
                    var id = 0;
                    foreach(var p in helper)
                    {
                        if (p.Nombre.Equals(proveedor))
                        {
                            id = p.Id;
                        }
                    }

                }

                var url_bod = "/api/bodegas";
                HttpResponseMessage new_bod = await client.GetAsync(url_bod);
                if (new_bod.IsSuccessStatusCode)
                {
                    var result = await new_bod.Content.ReadAsStringAsync();
                    dynamic a = JsonConvert.DeserializeObject(result);
                    List<ClsBodega> helper = new List<ClsBodega>();

                    foreach (var line in a)
                    {
                        Console.WriteLine(line);
                        ClsBodega bodega_a = new ClsBodega();
                        bodega_a.Id = line.ID_BODEGA;
                        bodega_a.Nombre = line.NOMBRE_BODEGA;
                        bodega_a.CodigoCorto = line.UBICACION_STOCK;
                        bodega_a.Direccion = line.DIRECCION;
                        bodega_a.Comuna = line.COMUNA;
                        helper.Add(bodega_a);
                    }
                    var id = 0;
                    foreach (var p in helper)
                    {
                        if (p.Nombre.Equals(bodega))
                        {
                            id = p.Id;
                        }
                    }

                }

                for (var a = 0; a<dataGridView1.RowCount; a++)
                {
                    Console.WriteLine(dataGridView1.Columns[a]);
                }
                //string url = "api/CrearCompra?proveedor=" + proveedor + "&bodega=" + bodega + "&fecha=" + fecha + "&imponible=" + {imponible} + "&impuesto=" + {impuesto} + "&total=" + {total} + "&producto=" + {producto} + "&cantidad=" + {cantidad} + "&precio_unitario=" + {precio_unitario} + "&subtotal=" + {subtotal} + "&total_detalle=" + {total_detalle};
                //HttpResponseMessage res = await client.PostAsync(url, null);
                //if (res.IsSuccessStatusCode)
                //{
                //    MessageBox.Show("El proveedor se Registro con Exitó");
                //}
                //else
                //{
                //    MessageBox.Show("Error! Ocurrio un problema al Guardar el Registro \n Contacte el Administrador del Sistema");
                //}
            }
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {

        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
