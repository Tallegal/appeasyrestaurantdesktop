﻿using CapaAccesoDatos;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaPresentacion.Administración
{
    public partial class CrearUsuario : Form
    {
        public string id;
        public HttpClient client = new HttpClient();

        public CrearUsuario()
        {
            InitializeComponent();
            client.BaseAddress = new Uri("http://192.168.1.163:49231/");
            //client.BaseAddress = new Uri("http://192.168.1.87:49231/");
        }

        private async void btnGuardar_Click(object sender, EventArgs e)
        {
            if(lblId.Text != "")
            {
                string nombre = string.Format("{0} {1} {2}", (txtNombre.Text), (txtApellidoP.Text), (txtApellidoM.Text));
                string telefono = txtTelefono.Text;
                string email = txtEmail.Text;
                string id = lblId.Text;
                string venta = "0";
                string compra = "0";
                string finanzas = "0";
                string bodega = "0";
                string cocina = "0";
                string administracion = "0";
                for (int a = 0; a < chkListaPermisos.CheckedItems.Count; a++)
                {
                    if (chkListaPermisos.CheckedItems[a].Equals("Ventas"))
                    {
                        venta = "1";
                    }
                    else if (chkListaPermisos.CheckedItems[a].Equals("Compras"))
                    {
                        compra = "1";
                    }
                    else if (chkListaPermisos.CheckedItems[a].Equals("Finanzas"))
                    {
                        finanzas = "1";
                    }
                    else if (chkListaPermisos.CheckedItems[a].Equals("Bodega"))
                    {
                        bodega = "1";
                    }
                    else if (chkListaPermisos.CheckedItems[a].Equals("Cocina"))
                    {
                        cocina = "1";
                    }
                    else if (chkListaPermisos.CheckedItems[a].Equals("Administración"))
                    {
                        administracion = "1";
                    }

                }


                var url = "api/UpdateUsuario?name=" + nombre + "&phone=" + telefono + "&email=" + email + "&id=" + id + "&venta=" + venta + "&compra=" + compra + "&finanzas=" + finanzas + "&bodega=" + bodega + "&cocina=" + cocina +"&administracion=" + administracion;

                HttpResponseMessage res = await client.PostAsync(url, null);
                
                if (res.IsSuccessStatusCode)
                {
                    MessageBox.Show("El Usuario se Modificó con Éxito");
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Error! Ocurrió un problema al Modificar el Registro \n Contacte con el Administrador del Sistema");
                }
            }
            else
            {
                string nombre = string.Format("{0} {1} {2}", (txtNombre.Text), (txtApellidoP.Text), (txtApellidoM.Text));
                string rut = txtRut.Text;
                string telefono = txtTelefono.Text;
                string email = txtEmail.Text;
                string venta = "0";
                string compra = "0";
                string finanzas = "0";
                string bodega = "0";
                string cocina = "0";
                string administracion = "0";
                for (int a = 0; a < chkListaPermisos.CheckedItems.Count; a++)
                {
                    if (chkListaPermisos.CheckedItems[a].Equals("Ventas"))
                    {
                        venta = "1";
                    }
                    else if (chkListaPermisos.CheckedItems[a].Equals("Compras"))
                    {
                        compra = "1";
                    }
                    else if (chkListaPermisos.CheckedItems[a].Equals("Finanzas"))
                    {
                        finanzas = "1";
                    }
                    else if (chkListaPermisos.CheckedItems[a].Equals("Bodega"))
                    {
                        bodega = "1";
                    }
                    else if (chkListaPermisos.CheckedItems[a].Equals("Cocina"))
                    {
                        cocina = "1";
                    }
                    else if (chkListaPermisos.CheckedItems[a].Equals("Administración"))
                    {
                        administracion = "1";
                    }

                }
                string url = "api/CrearUsuario?rut=" + rut + "&name=" + nombre + "&phone=" + telefono + "&email=" + email + "&venta=" + venta + "&compra=" + compra + "&finanzas=" + finanzas + "&bodega=" + bodega + "&cocina=" + cocina + "&administracion=" + administracion;
                HttpResponseMessage res = await client.PostAsync(url, null);
                if (res.IsSuccessStatusCode)
                {
                    MessageBox.Show("El Usuario se Registro con Exitó");
                }
                else
                {
                    MessageBox.Show("Error! Ocurrio un problema al Guardar el Registro \n Contacte el Administrador del Sistema");
                }
            }
            
            
        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private async void CrearUsuario_Load(object sender, EventArgs e)
        {
            
            if (this.id != null)
            {
                btnEditar.Visible = true;
                btnGuardar.Visible = false;
                var url = "/api/Usuarios/" + this.id;
                HttpResponseMessage message = await client.GetAsync(url);
                if (message.IsSuccessStatusCode)
                {
                    var result = await message.Content.ReadAsStringAsync();
                    Console.WriteLine(result);
                    dynamic json = JsonConvert.DeserializeObject(result);                 
                    
                    lblId.Text = json.ID_USUARIO;
                    
                    string[] nombre_split = json.NOMBRE.ToString().Split(' ');
                    txtNombre.Text = nombre_split[0];
                    if (nombre_split.Length > 1)
                    {
                        txtApellidoM.Text = nombre_split[1];
                    }
                    if(nombre_split.Length > 2)
                    {
                        txtApellidoP.Text = nombre_split[2];
                    }
                    txtEmail.Text = json.EMAIL;
                    txtTelefono.Text = json.TELEFONO;
                    txtRut.Text = json.RUT;
      
                    if (json.VENTA == "1")
                    {
                        chkListaPermisos.SetItemChecked(0, true);
                    }
                    if (json.COMPRA == "1")
                    {
                        chkListaPermisos.SetItemChecked(1, true);
                    }
                    if (json.FINANZAS == "1")
                    {
                        chkListaPermisos.SetItemChecked(2, true);
                    }
                    if (json.BODEGA == "1")
                    {
                        chkListaPermisos.SetItemChecked(3, true);
                    }
                    if (json.COCINA == "1")
                    {
                        chkListaPermisos.SetItemChecked(4, true);
                    }
                    if (json.ADMINISTRACION == "1")
                    {
                        chkListaPermisos.SetItemChecked(5, true);
                    }

                    

                    // Solo lectura
                    txtApellidoM.ReadOnly = true;
                    txtApellidoP.ReadOnly = true;
                    txtRut.ReadOnly = true;
                    txtNombre.ReadOnly = true;
                    txtEmail.ReadOnly = true;
                    txtTelefono.ReadOnly = true;
                }
                
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            
            btnEditar.Visible = false;
            btnGuardar.Visible = true;
            txtApellidoM.ReadOnly = false;
            txtApellidoP.ReadOnly = false;
            txtNombre.ReadOnly = false;
            txtEmail.ReadOnly = false;
            txtTelefono.ReadOnly = false;
        }

        private void chkListaPermisos_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
