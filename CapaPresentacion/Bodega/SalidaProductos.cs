﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaPresentacion
{
    public partial class SalidaProductos : Form
    {
        public SalidaProductos()
        {
            InitializeComponent();
        }

        private void btnCrearSalida_Click(object sender, EventArgs e)
        {
            MovimientoBodega mb = new MovimientoBodega();
            mb.ShowDialog();
        }
    }
}
