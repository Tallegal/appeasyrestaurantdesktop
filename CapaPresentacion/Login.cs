﻿using CapaAccesoDatos;
using CapaLogicaNegocios;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaPresentacion
{
    public partial class Login : Form
    {
      
        public Login()
        {
            InitializeComponent();
        }

        private void Login_Load(object sender, EventArgs e)
        {

        }

        private async void btnLogin_Click(object sender, EventArgs e)
        {
            if (txtRut.Text=="" || txtPassword.Text == "")
            {
                MessageBox.Show("Debe ingresar sus credenciales para Logearse al Sistema");
            }else
            {
                Inicio ini = new Inicio();
                ClsUsuario usuario_login = new ClsUsuario();
                HttpClient client = new HttpClient();
                //Encripta
                string resulta = string.Empty;
                byte[] encryted = System.Text.Encoding.Unicode.GetBytes(txtPassword.Text);
                resulta = Convert.ToBase64String(encryted);

                string result = null;
                string url = "http://192.168.1.163:49231/api/Login?rut=" + txtRut.Text + "&password=" + resulta;
                //string url = "http://192.168.0.22:49231/api/Login?rut=" + txtRut.Text + "&password=" + resulta;
                HttpResponseMessage res = await client.GetAsync(url);
                if (res.IsSuccessStatusCode)
                {
                    result = await res.Content.ReadAsStringAsync();
                }
                if (int.Parse(result) > 0)
                {
                    string url_usuario = "http://192.168.1.163:49231/api/ExisteUsuario?rut=" + txtRut.Text;
                    HttpResponseMessage message = await client.GetAsync(url_usuario);
                    if (message.IsSuccessStatusCode)
                    {
                        var result_dos = await message.Content.ReadAsStringAsync();
                        Console.WriteLine(result_dos);
                        dynamic json = JsonConvert.DeserializeObject(result_dos);

                        usuario_login.Id = json.ID_USUARIO;

                        usuario_login.Nombre = json.NOMBRE;
                        
                        usuario_login.Email = json.EMAIL;
                        usuario_login.Telefono = json.TELEFONO;
                        usuario_login.Rut = json.RUT;
                        usuario_login.Venta = json.VENTA;
                        usuario_login.Compra = json.COMPRA;
                        usuario_login.Finanzas = json.FINANZAS;
                        usuario_login.Bodega = json.BODEGA;
                        usuario_login.Cocina = json.COCINA;
                        usuario_login.Administracion = json.ADMINISTRACION;
                        ini.usuario_logeado = usuario_login;
                        ini.Show();

                    }
                    else
                    {
                        MessageBox.Show("Error! Usuario no existe | Contactese con el Administrador del Sistema");
                    }


                }
                else
                {
                    MessageBox.Show("Error! Credenciales invalidas | Contactese con el Administrador del Sistema");
                }

            }

        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
