﻿using CapaAccesoDatos;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaPresentacion
{
    public partial class Proveedores : Form
    {
        public HttpClient client = new HttpClient();

        public Proveedores()
        {
            InitializeComponent();
            client.BaseAddress = new Uri("http://192.168.1.163:49231/");
        }

        public async void poblarGridView()
        {
            string url = "/api/proveedores";
            HttpResponseMessage res = await client.GetAsync(url);
            if (res.IsSuccessStatusCode)
            {
                var result = await res.Content.ReadAsStringAsync();
                dynamic a = JsonConvert.DeserializeObject(result);
                List<ClsProveedor> helper = new List<ClsProveedor>();

                foreach (var line in a)
                {
                    Console.WriteLine(line);
                    ClsProveedor proveedor = new ClsProveedor();
                    proveedor.Id = line.ID_PROVEEDOR;
                    proveedor.Nombre = line.NOMBRE_PROVEEDOR;
                    proveedor.Rut = line.RUT_PROVEEDOR;
                    proveedor.Email = line.MAIL_PROVEEODR;
                    proveedor.Telefono = line.TELEFONO_PROVEEDOR;

                    helper.Add(proveedor);
                }
                int count = 1;
                foreach (ClsProveedor user in helper)
                {
                    int n = dataGridView1.Rows.Add();
                    dataGridView1.Rows[n].Cells[0].Value = user.Id;
                    dataGridView1.Rows[n].Cells[1].Value = user.Nombre;
                    dataGridView1.Rows[n].Cells[2].Value = user.Telefono;
                    dataGridView1.Rows[n].Cells[3].Value = user.Email;
                    count++;
                }

            }


        }

        private void btnCrearProveedor_Click(object sender, EventArgs e)
        {
            CrearProveedor cp = new CrearProveedor();
            cp.ShowDialog();
        }

        private void Proveedores_Load(object sender, EventArgs e)
        {
            poblarGridView();
        }

        private async void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView1.Columns[e.ColumnIndex].Name == "btnVisualizar")
            {
                CrearProveedor cu = new CrearProveedor();
                cu.id = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
                cu.Show();
            }
            if (dataGridView1.Columns[e.ColumnIndex].Name == "btnEliminar")
            {
                if (MessageBox.Show("Está seguro que desea eliminar un registro?", "Eliminar un Registro", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    var id_eliminar = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
                    var url = "api/proveedores/" + id_eliminar;
                    HttpClient client = new HttpClient();
                    client.BaseAddress = new Uri("http://192.168.1.163:49231/");
                    //client.BaseAddress = new Uri("http://192.168.1.87:49231/");
                    HttpResponseMessage res = await client.DeleteAsync(url);
                    if (res.IsSuccessStatusCode)
                    {
                        MessageBox.Show("Se elimino el Registro de manera Exitosa!");
                    }
                    else
                    {
                        MessageBox.Show("Ocurrio un problema al eliminar el Registro \n Contactese con el Administrador del Sistema");
                    }
                }

            }
        }

        private void btnActualizar_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();
            poblarGridView();
        }
    }
}
